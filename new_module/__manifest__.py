# -*- coding: utf-8 -*-
{
    'name' : 'Courses',
    
    'summary': 'Send Invoices and Track Payments',
    
    'description': """
Core mechanisms for the accounting modules. To display the menuitems, install the module account_invoicing.
    """,
    
    'website': 'https://www.odoo.com/page/billing',
    
    'depends' : [
        'base_setup',
        'crm',
        ],
    'data': [
        'views/studying_course_view.xml',
        'views/res_partner_view.xml',
        'security/course_security.xml',
        'security/ir.model.access.csv'
    ],
    'demo': [
        'data/course.xml',
    ],
    
    'installable': True,
    'application': False,
    'auto_install': False,
    
}
