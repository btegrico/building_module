# -*- coding: utf-8 -*-

from datetime import date
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError

# create other type class
# class Course(models.AbstractModel):
# class Course(models.TransientModel):


class Course(models.Model):
    
    _name = 'studying.course'
    
    # тільки додає маіл тред
    # _inherit = ['mail.thread']

    # додає тред і можливість створювати активності в треді
    _inherit = ['mail.thread', 'mail.activity.mixin']
    
    
    name = fields.Char(string="Name",)
    
    
    # реляційне поле дозволяє вибрати одного партнера
    user = fields.Many2one('res.partner', string="Teacher", track_visibility="onchange")
    course_type = fields.Selection([('free', 'No charge'),
                                    ('paid', 'Paid')],
                                   default='free')
    
    participants = fields.One2many('course.participants', 'course_id')
    
    # поле дати значення можна встановити дефолтним значенням
    date_start = fields.Date(string="Begin date", default=date.today())
    
    date_end = fields.Date(string="End date",)
    active = fields.Boolean(default="True")
    
    # @api.model
    # def create(self, vals):
    #     super(Course, self).create(vals)
    #     {'particiant': '...'}

    @api.multi
    def search_course(self):
        
        course_obj = self.env['studying.course'].sudo().search([])
        some = course_obj.mapped('participants.user_id.child_ids')
        print(some)
        
        
        

    @api.constrains('date_start')
    def _check_date(self):
        '''
        перевірка на коректність вказаних даних
        :return:
        '''
        for record in self:
            if record.date_end and record.date_end <= record.date_start:
                raise ValidationError(_('End date should be after start date!'))
            

class Participants(models.Model):
    
    _name = "course.participants"
    
 
    
    course_id = fields.Many2one('studying.course')
    
    user_id = fields.Many2one('res.partner')
    stage = fields.Selection(
        [('done','Finished'),
         ('left', 'Leaved')],
        string='State'
    )
    final_mark = fields.Integer(string='Mark')