# -*- coding: utf-8 -*-
from odoo import api, fields, models


class PartnerParticipants(models.Model):
    
    _inherit = 'res.partner'

    courses_count = fields.Integer('Total courses', compute="_get_courses")
    
    
    @api.multi
    def _get_courses(self):
        '''
        :return:
        '''
        for partner in self:
            partner.courses_count = len(
                self.env['course.participants'].
                    search([('user_id','=', partner.id)]))
            


